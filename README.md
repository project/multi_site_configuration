Multi Site Configuration
===================

Multi Site Configuration module help to manage the different configurations for individual domains.

Configuration:
--------------
Multi Site Configuration module alter the site configuration page:

Multi Site Configuration Features:
1. You can configure site setting for individual domins. Configuration like 'Site Name', 'Slogon', 'Email', 'Front page', '404 page', '403 page' can be diffent for individual domains.
2. This module allow you to set different  emails for different domains similary you can set individual configuration for each domain.

Configuration Path: /admin/config/system/site-information


Dependencies:
-------------
    -domain
